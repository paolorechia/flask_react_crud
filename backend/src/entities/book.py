# coding=utf-8

from sqlalchemy import Column, String

from marshmallow import Schema, fields

from .entity import Entity, Base


class Book(Entity, Base):
    __tablename__ = 'books'

    title = Column(String)
    description = Column(String)

    def __init__(self, title, description, long_description, created_by):
        Entity.__init__(self, created_by)
        self.title = title
        self.description = description
        self.long_description = long_description

class BookSchema(Schema):
    id = fields.Number()
    title = fields.Str()
    description = fields.Str()
    created_at = fields.DateTime()
    updated_at = fields.DateTime()
    last_updated_by = fields.Str()    
