from flask import Flask, url_for

app = Flask(__name__)

@app.route("/")
def home():
    return "Home"

@app.route('/hello')
def hello():
    return 'Hello, World'

with app.test_request_context():
    print(url_for('home'))
    print(url_for('hello'))
