"""Testing alembic

Revision ID: c85452a24f8b
Revises: 
Create Date: 2019-04-30 15:22:54.859728

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c85452a24f8b'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('books', sa.Column(
        'long_description',
        sa.Text,
        nullable=False,
        server_default='Default book long description'))
    pass


def downgrade():
    pass
