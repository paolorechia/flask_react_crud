import React from 'react';
import axios from 'axios';

export default class Book extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      books: [],
      newBookTitle: '',
      newBookDescription: ''
    }
  };


  componentDidMount() {
    axios.get('http://0.0.0.0:5000/books')
      .then( res => {
        const books = res.data;
        this.setState({ books });
      })
  }

  handleSubmit = event =>  {
    event.preventDefault();
    axios.post('http://0.0.0.0:5000/books',
      { 
        title: this.state.newBookTitle,
        description: this.state.newBookDescription
      })
      .then( res => {
        console.log(res);
        console.log(res.data);
      })
  }

  handleNewTitle = event => {
    this.setState({ newBookTitle: event.target.value }); 
  }

  handleNewDescription = event => {
    this.setState({ newDescription: event.target.value });
  }

  render() {
    return (
      <div className="BookList">
        <ul>
          { 
            this.state.books.map( book =>
            <li> {book.title} </li>)
          }
        </ul>
        <div> 
        <form onSubmit={this.handleSubmit}>
          <label>
            Book Title:
            <input type="text" name="title" onChange={this.handleNewTitle} />
          </label>
          <label>
            Book Description:
            <input type="text" name="description" onChange={this.handleNewDescription} />
          </label>
          <button type="submit"> Add button </button>
        </form>
        </div>
      </div>
    );
  }
}

