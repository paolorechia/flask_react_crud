import React from 'react';
import './App.css';
import BookList from './BookList.js';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <BookList />
      </header>
    </div>
  );
}

export default App;
